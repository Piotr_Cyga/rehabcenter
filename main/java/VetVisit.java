import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class VetVisit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    @ManyToOne
    @JoinColumn(name = "Animal_Id")
    private Animal animal;

    private Diagnosis diagnosis;

    //private Treatment treatment;

    private LocalDateTime dateAndTimeOfVisit;

    private double cost;
}
