import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "Animal_kind_ID")
    private AnimalKind animalKind;

    private LocalDateTime dateAndTimeOfAdmission;

    private Form form;

    private LocalDate ageInDaysOnAdmission;

    @OneToMany(mappedBy = "treatment_list")
    private VetVisit vetVisit;
}
